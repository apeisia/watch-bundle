<?php

namespace Apeisia\WatchBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ApeisiaWatchExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
        $directories = $config['directories'];
        if (!$directories) {
            $directories = ['%kernel.project_dir%/src', '%kernel.project_dir%/templates', '%kernel.project_dir%/vendor/apeisia', '%kernel.project_dir%/config'];
        }
        $container->setParameter('apeisia.aurelia_watch.scan_directories', $directories);
    }
}
