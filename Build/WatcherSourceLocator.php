<?php

namespace Apeisia\WatchBundle\Build;

use InvalidArgumentException;
use Roave\BetterReflection\Identifier\Identifier;
use Roave\BetterReflection\SourceLocator\Ast\Locator;
use Roave\BetterReflection\SourceLocator\Exception\InvalidFileLocation;
use Roave\BetterReflection\SourceLocator\FileChecker;
use Roave\BetterReflection\SourceLocator\Located\LocatedSource;
use Roave\BetterReflection\SourceLocator\Type\ComposerSourceLocator;

class WatcherSourceLocator extends ComposerSourceLocator
{

    /** @var string */
    private $fileName;

    /**
     * @throws InvalidFileLocation
     */
    public function __construct(string $fileName, Locator $astLocator, string $projectDir)
    {
        FileChecker::assertReadableFile($fileName);

        $loader = require $projectDir.'/vendor/autoload.php';

        parent::__construct($loader, $astLocator);

        $this->fileName = $fileName;
    }

    /**
     * {@inheritDoc}
     *
     * @throws InvalidArgumentException
     * @throws InvalidFileLocation
     */
    protected function createLocatedSource(Identifier $identifier) : ?LocatedSource
    {
        if ($identifier->getName() == Identifier::WILDCARD) {
            return new LocatedSource(
                file_get_contents($this->fileName),
                $this->fileName,
                $this->fileName,
            );
        }
        return parent::createLocatedSource($identifier);
    }
}
