<?php

namespace Apeisia\WatchBundle\Build;

use Apeisia\WatchBundle\Event\FileChangedEvent;
use Apeisia\WatchBundle\Event\FileDeletedEvent;
use Concerto\DirectoryMonitor\MonitorException;
use Concerto\DirectoryMonitor\RecursiveMonitor;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class Watcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var ProcessFileVoter
     */
    private $processFileVoter;
    /**
     * @var array
     */
    private $scanDirectories;
    /**
     * @var ProcessFileVoter
     */
    private $fileVoter;
    /**
     * @var LoopSingleton
     */
    private $loopSingleton;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        ProcessFileVoter $processFileVoter,
        ProcessFileVoter $fileVoter,
        LoopSingleton $loopSingleton,
        array $scanDirectories
    )
    {
        $this->eventDispatcher  = $eventDispatcher;
        $this->processFileVoter = $processFileVoter;
        $this->scanDirectories  = $scanDirectories;
        $this->fileVoter        = $fileVoter;
        $this->loopSingleton    = $loopSingleton;
    }

    /**
     * @throws MonitorException
     */
    public function run()
    {
        $loop     = $this->loopSingleton->getLoop();
        $backlog  = [];
        $debounce = function ($event, $path) use (&$backlog, $loop) {
            if (!$this->fileVoter->votePath($path)) return;

            if (array_key_exists($path, $backlog)) {
                $loop->cancelTimer($backlog[$path]);
            }
            $backlog[$path] = $loop->addTimer(0.1, function () use ($event, $path) {
                if ($event == 'delete') {
                    $this->eventDispatcher->dispatch(new FileDeletedEvent($path, false));
                } else {
                    $this->eventDispatcher->dispatch(new FileChangedEvent($path, false));
                }
            });
        };

        foreach ($this->scanDirectories as $directory) {
            if (!file_exists($directory)) continue;

            $monitor = new RecursiveMonitor($loop, $directory);
            $monitor->on('create', function ($path, $root) use ($debounce) {
                $debounce('create', $root . '/' . $path);
            });

            $monitor->on('delete', function ($path, $root) use ($debounce) {
                $debounce('delete', $root . '/' . $path);
            });

            $monitor->on('modify', function ($path, $root) use ($debounce) {
                $debounce('modify', $root . '/' . $path);
            });

            $monitor->on('write', function ($path, $root) use ($debounce) {
                $debounce('write', $root . '/' . $path);
            });
        }
        $loop->run();
    }
}
