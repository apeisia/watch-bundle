<?php

namespace Apeisia\WatchBundle\Build;

use React\EventLoop\Factory as EventLoopFactory;
use React\EventLoop\LoopInterface;

class LoopSingleton
{
    private $loop;

    public function __construct()
    {
        $this->loop = EventLoopFactory::create();
    }

    public function getLoop(): LoopInterface
    {
        return $this->loop;
    }
}
