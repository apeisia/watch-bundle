<?php

namespace Apeisia\WatchBundle\Build;

class ProcessFileVoter
{
    public function votePath($path)
    {
        $extensions = ['html', 'twig', 'ts', 'php', 'scss', 'yaml', 'proto'];
        $ext        = explode('.', $path);

        return in_array($ext[count($ext) - 1], $extensions) || is_dir($path);
    }
}

