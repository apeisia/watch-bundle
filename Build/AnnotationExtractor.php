<?php

namespace Apeisia\WatchBundle\Build;

use Apeisia\BaseBundle\Annotation\AnnotationAndAttributeReader;
use Apeisia\WatchBundle\Event\ClassChangedEvent;
use Apeisia\WatchBundle\Event\FileChangedEvent;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Roave\BetterReflection\BetterReflection;
use Roave\BetterReflection\Reflection\Adapter\ReflectionClass as ReflectionClassAdapter;
use Roave\BetterReflection\Reflector\DefaultReflector;
use Roave\BetterReflection\SourceLocator\Ast\Exception\ParseToAstFailure;
use Roave\BetterReflection\SourceLocator\Type\AggregateSourceLocator;
use Roave\BetterReflection\SourceLocator\Type\PhpInternalSourceLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class AnnotationExtractor implements EventSubscriberInterface
{
    private EventDispatcherInterface $eventDispatcher;
    private string $projectDir;

    public function __construct(EventDispatcherInterface $eventDispatcher, string $projectDir)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->projectDir      = $projectDir;
        
        if (method_exists(AnnotationRegistry::class, 'registerLoader')) {
            // this was removed and is no longer necessary in doctrine/annotations 2.0
            AnnotationRegistry::registerLoader('class_exists');
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FileChangedEvent::class => 'fileChanged',
        ];
    }

    /**
     * @param FileChangedEvent $ev
     * @throws \ReflectionException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function fileChanged(FileChangedEvent $ev)
    {
        if (substr($ev->getPath(), -4) != '.php') return;
        foreach ($this->extractFromFile($ev->getPath()) as [$class, $annotations]) {
            $this->eventDispatcher->dispatch(new ClassChangedEvent($class->getName(), $class, $annotations));
        }
    }

    /**
     * @param $file
     * @return array  [ [ReflectionClass, Annotations[]], ... ]
     * @throws \ReflectionException
     */
    public function extractFromFile($file)
    {
        $betterReflection = new BetterReflection();
        $astLocator       = $betterReflection->astLocator();
        $sourceStubber    = $betterReflection->sourceStubber();
        try {
            $reflector = new DefaultReflector(new AggregateSourceLocator([
                new WatcherSourceLocator($file, $astLocator, $this->projectDir),
                new PhpInternalSourceLocator($astLocator, $sourceStubber)
            ]));
            $classes   = $reflector->reflectAllClasses();

        } catch (ParseToAstFailure $e) {
            return []; // syntax error. do not process further
        }
        $reader = new AnnotationAndAttributeReader();
        $r      = [];
        foreach ($classes as $class) {
            $adapter     = new ReflectionClassAdapter($class);
            $annotations = $reader->getClassAnnotations($adapter);
            $r[]         = [$class, $annotations];
        }

        return $r;
    }
}

