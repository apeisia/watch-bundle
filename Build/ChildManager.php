<?php

namespace Apeisia\WatchBundle\Build;

use Apeisia\WatchBundle\Event\ChildProcessClassEvent;
use Apeisia\WatchBundle\Event\ChildProcessFileEvent;
use Apeisia\WatchBundle\Event\ClassChangedEvent;
use Apeisia\WatchBundle\Event\FileChangedEvent;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use React\ChildProcess\Process;
use Roave\BetterReflection\Reflection\Adapter\ReflectionClass as ReflectionClassAdapter;
use Roave\BetterReflection\Reflection\ReflectionClass as BetterReflectionClass;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ChildManager extends AsyncProcessRunner
{
    const MAX_CHILDREN = 10;

    private $backlog = [];
    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var bool
     */
    private $exitOnError = false;

    public function __construct(
        LoopSingleton $loopSingleton,
        KernelInterface $kernel,
        EventDispatcherInterface $eventDispatcher
    )
    {
        parent::__construct($loopSingleton);
        $this->kernel          = $kernel;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function enableExitOnError()
    {
        $this->exitOnError = true;
    }

    /**
     * @param ClassChangedEvent|FileChangedEvent $originalEvent
     * @param string $callingClass
     */
    public function executeChildClassEvent($originalEvent, string $callingClass)
    {
        $this->backlog[] = [$originalEvent, $callingClass];
        $this->startNext();
    }

    private function startNext()
    {
        if (count($this->backlog) == 0) return;
        if ($this->running == self::MAX_CHILDREN) return;
        /** @var ClassChangedEvent|FileChangedEvent $originalEvent */
        [$originalEvent, $callingClass] = array_shift($this->backlog);

        if ($originalEvent instanceof FileChangedEvent) {
            $type           = 'file';
            $eventParameter = $originalEvent->getPath();
        } else {
            $type           = 'class';
            $eventParameter = $originalEvent->getClassName();
        }

        $process = new Process(
            PHP_BINARY.' '.
            __DIR__ . '/child ' .
            escapeshellarg($type) . ' ' .
            escapeshellarg($callingClass) . ' ' .
            escapeshellarg($eventParameter),
            $this->kernel->getProjectDir(),
            getenv()
        );

        $process->on('exit', function ($exitCode)  {
            $this->startNext();
            if ($this->exitOnError && $exitCode != 0) {
                exit($exitCode);
            }
        });

        $this->start($process);
    }

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function childEntry()
    {
        AnnotationRegistry::registerLoader('class_exists');
        $eventType      = $_SERVER['argv'][1];
        $callingClass   = $_SERVER['argv'][2];
        $eventParameter = $_SERVER['argv'][3];
        if ($eventType == 'file') {
            $this->eventDispatcher->dispatch(
                new ChildProcessFileEvent($eventParameter, $callingClass)
            );
        } else {
            $reader      = new AnnotationReader();
            $class       = BetterReflectionClass::createFromName($eventParameter);
            $annotations = $reader->getClassAnnotations(new ReflectionClassAdapter($class));

            $this->eventDispatcher->dispatch(
                new ChildProcessClassEvent($class->getName(), $class, $annotations, $callingClass)
            );
        }

    }
}
