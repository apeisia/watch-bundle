<?php

namespace Apeisia\WatchBundle\Build;

use Apeisia\WatchBundle\Event\FileChangedEvent;
use Apeisia\WatchBundle\Event\FreshBuildEvent;
use Apeisia\WatchBundle\Event\InitialScanCompletedEvent;
use DirectoryIterator;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class RecursiveBuilder
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var array
     */
    private $scanDirectories;
    /**
     * @var ProcessFileVoter
     */
    private $fileVoter;

    public function __construct(EventDispatcherInterface $eventDispatcher, ProcessFileVoter $fileVoter, array $scanDirectories)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->scanDirectories = $scanDirectories;
        $this->fileVoter       = $fileVoter;
    }

    public function build($isBuildOnly)
    {
        $this->eventDispatcher->dispatch(new FreshBuildEvent($isBuildOnly));

        foreach ($this->scanDirectories as $directory) {
            if (!file_exists($directory)) continue;
            $this->directory($directory);
        }
        $this->eventDispatcher->dispatch(new InitialScanCompletedEvent());
    }

    private function directory($path)
    {
        foreach (new DirectoryIterator($path) as $file) {
            if ($file->isDot()) continue;
            if ($file->isDir()) {
                $this->directory($file->getPathname());
            } else {
                $this->eventDispatcher->dispatch(new FileChangedEvent($file->getPathname(), true));
            }
        }
    }
}
