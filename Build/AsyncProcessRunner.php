<?php

namespace Apeisia\WatchBundle\Build;

use React\ChildProcess\Process;

class AsyncProcessRunner
{

    /**
     * @var LoopSingleton
     */
    protected $loopSingleton;

    protected $running = 0;

    public function __construct(
        LoopSingleton $loopSingleton
    )
    {
        $this->loopSingleton = $loopSingleton;
    }
            
    protected function start(Process $process)
    {
        $this->running++;

        $process->on('exit', function ($exitCode) {
            $this->running--;
        });
        $process->start($this->loopSingleton->getLoop());
        $process->stdout->on('data', function ($chunk) {
            echo $chunk;
        });
        $process->stderr->on('data', function ($chunk) {
            echo $chunk;
        });
    }

    public function __destruct()
    {
        while ($this->running > 0) {
            $this->loopSingleton->getLoop()->tick();
            usleep(100000);
        }
    }
}
