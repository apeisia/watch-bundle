FROM php:8.1-bullseye AS php_watch

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions inotify

RUN set -x && \
    apt update && \
    apt install npm -y && \
    npm install -g prettier

ARG UID
ARG GID
RUN groupadd --gid=${GID} appuser && \
    useradd --shell=/bin/bash --create-home --home-dir=/home/appuser --gid=${GID} --uid=${UID} --no-log-init appuser
USER appuser

VOLUME /srv/api
WORKDIR /srv/api

CMD ["php", "-d", "memory_limit=1G", "/srv/api/vendor/apeisia/watch-bundle/console", "au:watch", "-v"]
