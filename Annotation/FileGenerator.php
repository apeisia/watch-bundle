<?php

namespace Apeisia\WatchBundle\Annotation;

/**
 * A class with this annotation must implement FrontendClassGeneratorInterface
 *
 * @Annotation
 * @Target({"CLASS"})
 */
class FileGenerator
{
    /**
     * @var string
     */
    public $outputFile;
}
