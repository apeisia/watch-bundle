<?php

namespace Apeisia\WatchBundle\Annotation;

use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class AnnotationAndAttributeReader
{
    private AnnotationReader $reader;

    public function __construct()
    {
        $this->reader = new AnnotationReader();
    }

    public function getClassAnnotations(ReflectionClass $class): array
    {
        return $this->reader->getClassAnnotations($class) + $this->initAttributes($class->getAttributes());
    }

    public function getClassAnnotation(ReflectionClass $class, string $className)
    {
        foreach ($this->getClassAnnotations($class) as $annotation) {
            if ($annotation instanceof $className) return $annotation;
        }
        return null;
    }

    public function getPropertyAnnotations(ReflectionProperty $property): array
    {
        return $this->reader->getPropertyAnnotations($property) + $this->initAttributes($property->getAttributes());
    }

    public function getPropertyAnnotation(ReflectionProperty $property, string $className)
    {
        foreach ($this->getPropertyAnnotations($property) as $annotation) {
            if ($annotation instanceof $className) return $annotation;
        }
        return null;
    }

    public function getMethodAnnotations(ReflectionMethod $method): array
    {
        return $this->reader->getMethodAnnotations($method) + $this->initAttributes($method->getAttributes());
    }

    public function getMethodAnnotation(ReflectionMethod $method, string $className)
    {
        foreach ($this->getMethodAnnotations($method) as $annotation) {
            if ($annotation instanceof $className) return $annotation;
        }
        return null;
    }

    private function initAttributes($attributes): array
    {
        return map($attributes, fn(ReflectionAttribute $attribute
        ) => new ($attribute->getName())(...$attribute->getArguments()));
    }
}
