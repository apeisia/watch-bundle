<?php

namespace Apeisia\WatchBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class InitialScanCompletedEvent extends Event
{
}
