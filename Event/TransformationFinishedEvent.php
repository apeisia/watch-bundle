<?php

namespace Apeisia\WatchBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class TransformationFinishedEvent extends Event
{
    private string $transformerClass;
    private string $outputFilename;
    private string $content;

    public function __construct(string $transformerClass, string $outputFilename, string $content)
    {
        $this->transformerClass = $transformerClass;
        $this->outputFilename   = $outputFilename;
        $this->content          = $content;
    }

    public function getTransformerClass(): ?string
    {
        return $this->transformerClass;
    }

    public function getOutputFilename(): ?string
    {
        return $this->outputFilename;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

}
