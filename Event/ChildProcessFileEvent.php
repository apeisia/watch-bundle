<?php

namespace Apeisia\WatchBundle\Event;

class ChildProcessFileEvent extends FileChangedEvent
{

    /**
     * @var string
     */
    private $callingClass;

    public function __construct(string $path, string $callingClass, bool $isFreshBuild = false)
    {
        parent::__construct($path, $isFreshBuild);
        $this->callingClass = $callingClass;
    }

    public function getCallingClass(): ?string
    {
        return $this->callingClass;
    }

}
