<?php

namespace Apeisia\WatchBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class FreshBuildEvent extends Event
{
    private $isBuildOnly;

    public function __construct($isBuildOnly)
    {
        $this->isBuildOnly = $isBuildOnly;
    }

    public function isBuildOnly()
    {
        return $this->isBuildOnly;
    }
}
