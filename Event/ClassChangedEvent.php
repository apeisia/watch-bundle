<?php

namespace Apeisia\WatchBundle\Event;

use Roave\BetterReflection\Reflection\Adapter\ReflectionClass as ReflectionClassAdapter;
use Roave\BetterReflection\Reflection\ReflectionClass as BetterReflectionClass;
use Symfony\Contracts\EventDispatcher\Event;

class ClassChangedEvent extends Event
{
    private string $className;
    private BetterReflectionClass $reflectionClass;
    private array $annotations;

    public function __construct(string $className, BetterReflectionClass $reflectionClass, array $annotations)
    {
        $this->className       = $className;
        $this->reflectionClass = $reflectionClass;
        $this->annotations     = $annotations;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getBetterReflectionClass(): BetterReflectionClass
    {
        return $this->reflectionClass;
    }

    public function getReflectionClass(): \ReflectionClass
    {
        return new ReflectionClassAdapter($this->reflectionClass);
    }

    /**
     * @return object[]
     */
    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    public function hasAnnotation(string $annotationClass)
    {
        foreach ($this->getAnnotations() as $annotation) {
            if ($annotation instanceof $annotationClass)
                return true;
        }

        return false;
    }

    public function getAnnotation(string $annotationClass)
    {
        foreach ($this->getAnnotations() as $annotation) {
            if ($annotation instanceof $annotationClass)
                return $annotation;
        }

        return null;
    }
}
