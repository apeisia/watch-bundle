<?php

namespace Apeisia\WatchBundle\Event;

use Roave\BetterReflection\Reflection\ReflectionClass as BetterReflectionClass;

class ChildProcessClassEvent extends ClassChangedEvent
{

    /**
     * @var string
     */
    private $callingClass;

    public function __construct(string $className, BetterReflectionClass $reflectionClass, array $annotations, string $callingClass)
    {
        parent::__construct($className, $reflectionClass, $annotations);
        $this->callingClass = $callingClass;
    }

    public function getCallingClass(): ?string
    {
        return $this->callingClass;
    }

}
