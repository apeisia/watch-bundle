<?php

namespace Apeisia\WatchBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class FileChangedEvent extends Event
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var bool
     */
    private $isFreshBuild;

    public function __construct(string $path, bool $isFreshBuild)
    {
        $this->path         = $path;
        $this->isFreshBuild = $isFreshBuild;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function isFreshBuild(): ?bool
    {
        return $this->isFreshBuild;
    }


}
