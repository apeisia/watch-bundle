<?php

namespace Apeisia\WatchBundle\Command;

use Apeisia\AureliaBundle\FormType\AureliaEntityType;
use Apeisia\WatchBundle\Build\RecursiveBuilder;
use Apeisia\WatchBundle\Build\Watcher;
use Apeisia\WatchBundle\Event\FileChangedEvent;
use Apeisia\WatchBundle\Event\FileDeletedEvent;
use Concerto\DirectoryMonitor\MonitorException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class WatchCommand extends Command
{

    /**
     * @var RecursiveBuilder
     */
    private $builder;

    /**
     * @var Watcher
     */
    private $watcher;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(RecursiveBuilder $builder, Watcher $watcher, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct();
        $this->builder         = $builder;
        $this->watcher         = $watcher;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function configure()
    {
        $this->setName('au:watch');
        $this->setDescription('Event emitting file watcher');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->builder->build(false);

        $this->eventDispatcher->addListener(FileChangedEvent::class, function (FileChangedEvent $event) use ($output) {
            $output->writeln('[M] ' . $event->getPath());
        }, 999);
        $this->eventDispatcher->addListener(FileDeletedEvent::class, function (FileDeletedEvent $event) use ($output) {
            $output->writeln('[D] ' . $event->getPath());
        }, 999);

        try {
            $this->watcher->run();
        } catch (MonitorException $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
            return 1;
        }

        return 0;
    }
}
