<?php

namespace Apeisia\WatchBundle\Command;

use Apeisia\AureliaBundle\FormType\AureliaEntityType;
use Apeisia\WatchBundle\Build\ChildManager;
use Apeisia\WatchBundle\Build\RecursiveBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BuildCommand extends Command
{
    /**
     * @var RecursiveBuilder
     */
    private $builder;
    /**
     * @var ChildManager
     */
    private $childManager;

    public function __construct(RecursiveBuilder $builder, ChildManager $childManager)
    {
        parent::__construct();
        $this->builder      = $builder;
        $this->childManager = $childManager;
    }

    public function configure()
    {
        $this->setName('au:build');
        $this->setDescription('Event emitting file builder');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->childManager->enableExitOnError();
        $this->builder->build(true);
        
        return 0;
    }
}
