<?php

namespace Apeisia\WatchBundle;

use Apeisia\AccessorTraitBundle\ApeisiaAccessorTraitBundle;
use Apeisia\ClientGeneratorBundle\ApeisiaClientGeneratorBundle;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

//    private const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            ApeisiaWatchBundle::class,
        ];
        if (class_exists(SensioFrameworkExtraBundle::class)) {
            $bundles[] = SensioFrameworkExtraBundle::class;
        }
        if (class_exists(ApeisiaAccessorTraitBundle::class)) {
            $bundles[] = ApeisiaAccessorTraitBundle::class;
        }
        if (class_exists(ApeisiaClientGeneratorBundle::class)) {
            $bundles[] = ApeisiaClientGeneratorBundle::class;
        }
        foreach ($bundles as $class) {
            yield new $class();
        }
    }

    public function getProjectDir(): string
    {
        return __DIR__ . '/../../..';
    }

    public function getCacheDir(): string
    {
        return $this->getProjectDir() . '/var/cache/watch-' . $this->environment;
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
        //$container->addResource(new FileResource($this->getProjectDir().'/config/bundles.php'));
        $container->setParameter('container.dumper.inline_class_loader', true);

        $loader->load(__DIR__ . '/Resources/config/services.yaml');
        $projectConfig = $this->getProjectDir() . '/config/watch-bundle.yaml';

        if (file_exists($projectConfig)) {
            $loader->load($projectConfig);
        }
    }
//
//    protected function configureRoutes(RouteCollectionBuilder $routes): void
//    {
//        $confDir = $this->getProjectDir().'/config';
//
//        $routes->import($confDir.'/{routes}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, '/', 'glob');
//        $routes->import($confDir.'/{routes}/*'.self::CONFIG_EXTS, '/', 'glob');
//        $routes->import($confDir.'/{routes}'.self::CONFIG_EXTS, '/', 'glob');
//    }
}
