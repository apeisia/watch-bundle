<?php

namespace Apeisia\WatchBundle\Transformer;

interface FileGeneratorInterface
{
    public static function getGeneratedContent(): string;
}
