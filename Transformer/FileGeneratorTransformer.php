<?php

namespace Apeisia\WatchBundle\Transformer;

use Apeisia\WatchBundle\Annotation\FileGenerator;
use Apeisia\WatchBundle\Build\ChildManager;
use Apeisia\WatchBundle\Event\ChildProcessClassEvent;
use Apeisia\WatchBundle\Event\ClassChangedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FileGeneratorTransformer implements EventSubscriberInterface
{
    /**
     * @var ChildManager
     */
    private $childManager;


    public function __construct(
        ChildManager $childManager
    )
    {
        $this->childManager = $childManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ClassChangedEvent::class      => 'classChanged',
            ChildProcessClassEvent::class => 'classChild',
        ];
    }

    public function classChanged(ClassChangedEvent $ev)
    {
        if (!$ev->hasAnnotation(FileGenerator::class)) return;
        $this->childManager->executeChildClassEvent($ev, self::class);
    }

    public function classChild(ChildProcessClassEvent $ev)
    {
        if ($ev->getCallingClass() != self::class) return;
        $outputFile = $ev->getAnnotation(FileGenerator::class)->outputFile;
        if (!$outputFile) return;
        $content = ([$ev->getClassName(), 'getGeneratedContent'])();
        file_put_contents($outputFile, $content);
    }
}
