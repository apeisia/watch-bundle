<?php

namespace Apeisia\WatchBundle\Output;

use Apeisia\WatchBundle\Event\TransformationFinishedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ResultFileWriter implements EventSubscriberInterface
{

    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            TransformationFinishedEvent::class => ['transformationFinished', 99],
        ];
    }

    public function transformationFinished(TransformationFinishedEvent $ev)
    {
        $dir = dirname($ev->getOutputFilename());
        if (substr($dir, 0, 1) === '@') {
            $dir = $this->kernel->locateResource($dir);
        }

        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        
        file_put_contents($dir . '/' . basename($ev->getOutputFilename()), $ev->getContent());
    }
}
